const outline = document.getElementById('reference-outline');
const overview = document.getElementById('reference-overview');
const mainReference = document.getElementById('reference-main');

const mainSections = mainReference.querySelectorAll('section');

for (const category of ['private', 'internal', 'inherited']) {
  const relevantFields = document.querySelectorAll(`.${category}`);
  const relevantFieldsOutline = document.querySelectorAll(`#reference-outline .${category}`);
  const toggle = document.getElementById(`reference-outline-show${category}`);
  const callback = toggleFields.bind(this, relevantFields, relevantFieldsOutline, toggle);

  toggle.addEventListener('input', callback);
  callback();
}


const intersectionElements = new Map();
observeViewportVisibility();


// ---- Viewport intersection observation ----
function observeViewportVisibility() {
  const observer = new IntersectionObserver(onIntersection, {
    rootMargin: '-8% 0px -20%',
    threshold: .05
  });
  mainSections.forEach(item => observer.observe(item));
}

function onIntersection(entries) {
  if (outline.offsetWidth === 0 && overview.offsetWidth === 0) return;

  for (let i = 0; i < entries.length; i++) {
    const entry = entries[i];
    intersectionElements.set(entry.target, entry.isIntersecting);

    // Hide the parent if all TypeAlias sections aren't visible anymore
    if (entry.target.classList.contains('typealias')
      && intersectionElements.get(entry.target.parentNode) === true
    ) {
      const parentID = entry.target.parentNode.querySelector('h2, h3').id;
      const noTypeIsVisible = Array.from(entry.target.parentNode.children)
        .filter(child => child.classList.contains('typealias'))
        .every(child => intersectionElements.get(child) === false);
      checkSidebarForID(outline, parentID, !noTypeIsVisible);
    } else {
      const header = entry.target.querySelector('h2, h3, h4');
      if (header) {
        checkSidebarForID(overview, header.id, entry.isIntersecting, i === 0);
        checkSidebarForID(outline, header.id, entry.isIntersecting);
      }
    }
  }
}

function checkSidebarForID(sidebar, id, isVisible, scrollIntoView = false) {
  const item = sidebar.querySelector(`[data-reference-id="${id}"]`);
  if (item) {
    if (isVisible) {
      item.classList.add('active');
      if (scrollIntoView) {
        item.scrollIntoView({ block: 'nearest' });
      }
    } else {
      item.classList.remove('active');
    }
  }
}

// ---- Outline field visibility ----
function toggleFields(elements, elementsOutline, targetInput) {
  for (const elem of elements) {
    elem.classList[targetInput.checked ? 'remove' : 'add']('hidden');
  }

  const handledSections = [];
  for (const elem of elementsOutline) {
    const section = elem.parentNode.parentNode;

    if (!handledSections.includes(section)) {
      section.classList[everySiblingIsHidden(elem) ? 'add' : 'remove']('hidden');
      handledSections.push(section);
    }
  }

  function everySiblingIsHidden(element) {
    return Array.from(element.parentNode.children)
      .every(sibling => sibling.classList.contains('hidden'));
  }
}
