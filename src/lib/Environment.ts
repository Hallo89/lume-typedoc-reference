import MarkdownIt from 'markdown-it';
import MarkdownItHighlightJS from 'markdown-it-highlightjs';
import Nunjucks from 'nunjucks';
import type { TypeDocDescriptor } from './TypedocJSONMutator.js';
import type { Controller } from './Controller.js';
import { TypedocJSONMutator } from './TypedocJSONMutator.js';
import { IndentationHandler } from './IndentationHandler.js';

/**
 * Handles the necessary operations needed for a template to be built.
 * Significant fields:
 * - A TypeDoc object that is mutated *in place* to fit the template's needs.
 * - A new Nunjucks environment with all filters needed by the template
 * @see {@link TypedocJSONMutator}
 * @see {@link IndentationHandler}
 */
export class Environment {
  /**
   * Controls the default visibility options (exposed through the checkboxes
   * on the top right) of the resulting documentation front-end.
   * @default
   * {
   *   private: false,
   *   internal: false,
   *   inherited: false,
   * }
   */
  visibility = {
    /** Show or hide all private fields. */
    private: false,
    /** Show or hide all fields marked with the "@internal" tag. */
    internal: false,
    /** Show or hide all inherited fields. */
    inherited: false
  };

  /**
   * A new [Nunjucks](https://mozilla.github.io/nunjucks/api.html) environment
   * that's used to compile the reference template.
   */
  njk: any;

  /**
   * A new [Markdown-It](https://github.com/markdown-it/markdown-it) environment
   * that's used to render inline markdown via the "md" filter.
   */
  md: any;

  /**
   * The supplied TypeDoc JSON as JavaScript object.
   * 
   * *IMPORTANT*: It is heavily mutated *in place*, so if you need to use the
   * intact object after building a doc from it, you will need to make a deep copy.
   */
  json: object;

  mutator: TypedocJSONMutator;
  indentHandler: IndentationHandler;

  
  constructor(json: object, nunjucksDirPath: string) {
    this.resolveTarget = this.resolveTarget.bind(this);

    this.njk = Nunjucks.configure(nunjucksDirPath);
    this.md = MarkdownIt({
      html: true
    });
    this.md.use(MarkdownItHighlightJS);

    this.json = json;

    this.mutator = new TypedocJSONMutator(json);
    this.indentHandler = new IndentationHandler();

    this.mutateJSON();
    this.addMandatoryFilters();
  }

  // ---- Renderers ----
  /** @see {@link Controller.build} */
  render(): string {
    return this.#renderTemplateHelper('doc.njk');
  }
  /** @see {@link Controller.buildMain} */
  renderMain(): string {
    return this.#renderTemplateHelper('main.njk');
  }
  /** @see {@link Controller.buildOverview} */
  renderOverview(): string {
    return this.#renderTemplateHelper('overview.njk');
  }
  /** @see {@link Controller.buildOutline} */
  renderOutline(): string {
    return this.#renderTemplateHelper('outline.njk');
  }

  #renderTemplateHelper(templateName: string): string {
    return this.njk.render(templateName, {
      visibility: this.visibility,
      json: this.json,
    });
  }


  /**
   * Call all predefined mutators. The order is important.
   * @see {@link TypedocJSONMutator}
   */
  mutateJSON() {
    this.mutator.markDeclarationsInternal();
    this.mutator.replaceReflectionKind();
    this.mutator.renameNamedParameters();
    this.mutator.numberMultipleInternals();
    this.mutator.orderTypeAliasToTop();
    this.mutator.mergeSignatureDocs();
    this.mutator.resolveDescriptors();
    this.mutator.resolveInheritance();
    this.mutator.orderExtendedBy();
    this.mutator.sortByHierarchy();
    this.mutator.orderProjectChildren();
    this.mutator.addVariantFieldCollection();
  }

  /** Add filters that are mandatory for the pre-defined Nunjucks templates to work. */
  addMandatoryFilters() {
    this.njk.addFilter('md', (val: string) => {
      val = val.toString().trim();
      return val && this.md.render(val);
    });

    this.njk.addFilter('typeof', (val: any) => typeof val);

    this.njk.addFilter('resolveTarget', this.resolveTarget);
    this.njk.addFilter('resolveRef', (target: number) => {
      return this.resolveTarget(target)?.reference;
    });

    this.njk.addFilter('addIndent', this.indentHandler.addIndent);

    this.njk.addFilter('isSingleWord', (val: string) => !val.includes(' '));
    this.njk.addFilter('removeFirstLetter', (val: string) => val.slice(1));
    this.njk.addFilter('camelToWords', (val: string) => {
      return val.replace(/.([A-Z])/g, (match, letter) => {
        return match.slice(0, 1) + ' ' + letter.toLowerCase();
      });
    });
  }

  // ---- Misc filters ----
  /** Get the target descriptor for the specified ID. */
  resolveTarget(target: number): TypeDocDescriptor | undefined {
    return this.mutator.targetByID.get(target);
  }
}
