/**
 * Simple helper class for resolving "hierarchy chains".
 *
 * A chain is a representation of a linear hierarchy; Every item can have
 * at most one parent and one child. Upon addition, they will be resolved
 * to a conclusive hierarchy chain, represented by an array.
 *
 * The representation of an item can be anything, but needs to be consistent
 * (generic class parameter T).
 */
export class Chain {
    #chains = new Set();
    /**
     * Add an item along with its parent.
     * The algorithm will add them to a (potentially pre-existing) chain.
     * @returns The chain that the items were added onto.
     *          NOTE that these chains may be deleted in successive calls to `add()`!
     *          It is good practice to use a WeakSet or WeakMap when storing them somewhere.
     */
    add(item, parent) {
        const itemChain = this.getChainFromItem(item);
        const parentChain = this.getChainFromItem(parent);
        // item can only ever be at the end of another chain,
        // parent can only ever be at the start of another chain
        if (itemChain) {
            itemChain.push(parent);
            if (parentChain) {
                itemChain.push(...parentChain.slice(1));
                this.#chains.delete(parentChain);
            }
            return itemChain;
        }
        else if (parentChain) {
            parentChain.unshift(item);
            return parentChain;
        }
        else {
            const chain = [item, parent];
            this.#chains.add(chain);
            return chain;
        }
    }
    /** Get the set of all chains. */
    get() {
        return this.#chains;
    }
    /** Find the chain (or false) from a given item. */
    getChainFromItem(item) {
        for (const chain of this.#chains) {
            if (chain.includes(item)) {
                return chain;
            }
        }
        return false;
    }
}
