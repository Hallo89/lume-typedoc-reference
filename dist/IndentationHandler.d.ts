import { HTMLElement, TextNode, Node } from 'node-html-parser';
/**
 * Contains methods to dynamically wrap & indent specific DOM elements
 * (namely containers like arrays, objects, etc.) once they exceed a specified width.
 * @see {@link indentLengthThreshold} for a way to modify the tested length.
 */
export declare class IndentationHandler {
    #private;
    /**
     * Classes that should be indented.
     * Is prioritized from top to bottom.
     */
    static indentableBodies: string[];
    /**
     * Classes that don't depend on added breaks in between
     * their child elements.
     */
    static independentBodies: string[];
    /**
     * Classes that may be collapsed, e.g. the array in
     * `[{ index: 1 }]`
     */
    static collapsibleBodies: string[];
    /**
     * Character length threshold dictating above which length
     * a method should be broken up and indented.
     */
    indentLengthThreshold: number;
    /**
     * Character length threshold dictating when an indent should
     * be avoided if its content is of up to this length.
     */
    indentAvoidThreshold: number;
    constructor();
    /**
     * Add indent to the specified text content.
     * The text content must be a valid, parsable HTML string.
     */
    addIndent(content: {
        val: string;
    }): string;
    static recursivelyIndentNode(node: Element, currentClass: string, avoidThreshold: number, lines: string[]): true | undefined;
    /** Checks whether an element is indentable as per {@link indentableBodies} */
    static isBody(element: Element | null): boolean | null;
    /** Checks whether an element is independent as per {@link independentBodies} */
    static isIndependentBody(element: Element | null): boolean | null;
    /** Checks whether an element is collapsible as per {@link collapsibleBodies} */
    static isCollapsibleBody(element: Element | null): boolean | null;
    /** Insert a <br> element between each of the supplied element's children. */
    static addIndentationBreaks(parentElement: Element): void;
    /** Wrap the child content of the supplied element in an `<div class=indent>` tag. */
    static addIndentationWrapper(element: Element): void;
    /**
     * Get a list of all lines that go over the line length threshold
     * from an arbitrary string input.
     */
    static getBreakableLines(input: string, threshold: number): string[];
    /**
     * Remove any newlines, insert newlines after <br> tags
     * and compress whitespace.
     *
     * This is done because the indenter uses newlines as markers to determine
     * line breaks. The whitespace is compressed to prevent length inaccuracies.
     */
    static getModifiedHTMLString(input: string): string;
    static postProcessHTMLString(input: string): string;
    static modifyStringAndCreateElement(content: string): Element;
    static before(element: Node, insertedElem: Node): void;
    static after(element: Node, insertedElem: Node): void;
    static createElement(type: string, classes?: string): HTMLElement;
    static createTextNode(content: string): TextNode;
    /** Get only the HTMLElements from a supplied elements childNodes. */
    static children(element: HTMLElement): HTMLElement[];
    static childElementCount(element: HTMLElement): number;
    static firstElementChild(element: HTMLElement): HTMLElement | undefined;
    static lastElementChild(element: HTMLElement): HTMLElement | undefined;
}
