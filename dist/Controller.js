import path from 'node:path';
import { Environment } from './Environment.js';
/** Path to the trdoc project root */
export const PROJECT_ROOT = path.join(import.meta.dirname, '../..');
/** Handles JSON mutation, Lume environments, DOM parsing, etc. */
export class Controller {
    /** @see {@link Options.cssDestFile} */
    cssDestFile;
    /** local path to the folder containing all Nunjucks templates. */
    nunjucksDirPath;
    /** local path to the trdoc CSS file. */
    cssFilePath;
    /** @see {@link Options.envInit} */
    envInit;
    /** A {@link Environment} created the potentially supplied {@link Options.defaultJSON}. */
    defaultEnv;
    constructor(config = {}) {
        this.build = this.build.bind(this);
        this.jsonPageLoader = this.jsonPageLoader.bind(this);
        this.envInit = config.envInit;
        this.cssDestFile = config.cssDestFile;
        this.nunjucksDirPath = path.join(PROJECT_ROOT, 'nunjucks/');
        this.cssFilePath = path.join(PROJECT_ROOT, 'style/reference.css');
        if (config.defaultJSON) {
            this.defaultEnv = this.createEnvironment(config.defaultJSON);
        }
        config.init?.(this);
    }
    /**
     * Get the rendered HTML string from the supplied TypeDoc object
     * or the configured default object if not supplied.
     *
     * This will render the actual reference in a <main> and
     * both the overview and the outline in an <aside>.
     * @see {@link Options.defaultJSON}
     * @param json A TypeDoc JSON as JavaScript object.
     */
    build(json) {
        return this.getEnv(json).render();
    }
    /** Same as {@link build}, but only render the main part of the reference. */
    buildMain(json) {
        return this.getEnv(json).renderMain();
    }
    /** Same as {@link build}, but only render the reference overview. */
    buildOverview(json) {
        return this.getEnv(json).renderOverview();
    }
    /** Same as {@link build}, but only render the reference outline. */
    buildOutline(json) {
        return this.getEnv(json).renderOutline();
    }
    /**
     * Get the rendered HTML string from a TypeDoc JSON file.
     * @param content The un-parsed content of the TypeDoc JSON file.
     */
    buildFromFileContent(content) {
        const json = JSON.parse(content);
        return this.build(json);
    }
    // ---- Helpers ----
    /**
     * Lume [page loader](https://lume.land/docs/core/loaders/),
     * rendering the contents of the supplied TypeDoc JSON path.
     * @param content The un-parsed content of the TypeDoc JSON file.
     */
    jsonPageLoader(content) {
        return {
            content: this.buildFromFileContent(content)
        };
    }
    /**
     * Create an {@link Environment} with the supplied TypeDoc object as base.
     * Calls {@link envInit} with the created environment for further customization.
     * @param json The TypeDoc JSON as JavaScript object.
     */
    createEnvironment(json) {
        const env = new Environment(json, this.nunjucksDirPath);
        this.envInit?.(env);
        return env;
    }
    /**
     * Get either a newly created {@link Environment} from the supplied TypeDoc
     * object or return the default Environment if nothing is passed.
     * @throws Throws if nothing has been passed and no default Env exists.
     * @param json The TypeDoc JSON as JavaScript object.
     */
    getEnv(json) {
        if (!json && !this.defaultEnv) {
            throw new Error('lume-typedoc-reference @ build() (`reference.*` component): '
                + 'No TypeDoc object has been passed, but no default JSON was configured (see Options.defaultJSON).');
        }
        return json ? this.createEnvironment(json) : this.defaultEnv;
    }
}
