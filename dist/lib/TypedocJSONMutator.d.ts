import { Chain } from './Chain.js';
/** Literally copied from [the TypeDoc GitHub](https://raw.githubusercontent.com/TypeStrong/typedoc/v0.25.13/src/lib/models/reflections/kind.ts) */
export declare enum ReflectionKind {
    Project = 1,
    Module = 2,
    Namespace = 4,
    Enum = 8,
    EnumMember = 16,
    Variable = 32,
    Function = 64,
    Class = 128,
    Interface = 256,
    Constructor = 512,
    Property = 1024,
    Method = 2048,
    CallSignature = 4096,
    IndexSignature = 8192,
    ConstructorSignature = 16384,
    Parameter = 32768,
    TypeLiteral = 65536,
    TypeParameter = 131072,
    Accessor = 262144,
    GetSignature = 524288,
    SetSignature = 1048576,
    TypeAlias = 2097152,
    Reference = 4194304
}
type MutationIteratorCallback = (args: MutationIteratorResult) => void;
interface MutationIteratorResult {
    /**
     * The currently resulting object.
     * It is always an object and never an array.
     */
    item: any;
    /**
     * The last object that was yielded.
     *
     * This is not necessarily the immediate parent of {@link item}
     * in the object hierarchy. Use {@link parentItem} for that.
     */
    parentObject: any;
    /**
     * The object that's {@link item}'s immediate parent.
     *
     * => `parentItem[itemKey] == item`.
     * @see {@link itemKey}
     */
    parentItem: any;
    /**
     * The index key of the current {@link item}. Can be used to delete the item.
     *
     * => `parentItem[itemKey] == item`.
     * @see {@link parentItem}
     */
    itemKey: string;
}
/**
 * Descriptor of a TypeDoc object with a link to the actual object.
 *
 * It's an extended version of the objects under `symbolIdMap` in a TypeDoc JSON
 * (https://typedoc.org/api/interfaces/JSONOutput.ReflectionSymbolId.html).
 */
export interface TypeDocDescriptor {
    sourceFileName: string;
    qualifiedName: string;
    /**
     * Optional custom identifier of the object.
     *
     * Currently(tm) (could change), its only possible value is "constructor".
     *
     * Used for the ID/hash link, i.e. `someParentName^constructor`
     * instead of `someParentName^new Slider89`
     */
    customName?: string;
    /**
     * How the parent of the object is addressed.
     * Accumulates hierarchy with dot-notation.
     *
     * Will not be built in case the object is a project.
     *
     * Used for the ID/hash link, i.e. `Properties.Base^someName`
     * for a hierarchy of `interface Properties` -> `interface Base` -> `someName`.
     */
    parentName?: string;
    /** Reference to the actual object of this descriptor. */
    reference: object;
    /** Reference to the immediate parent object of the represented object. */
    parentReference: object;
}
/** Contains methods that mutate the typedoc JSON object. */
export declare class TypedocJSONMutator {
    #private;
    json: object;
    /** Contains all {@link TypeDocDescriptor}s indexed by their ID. */
    targetByID: Map<number, TypeDocDescriptor>;
    /**
     * ID that can be used when registering new reflections.
     * Is currently unused but might prove useful, who knows.
     *
     * @remarks
     * It is your responsibility to decrement it after using it!
     *
     * @remarks
     * It starts this low because TypeDoc itself uses the id `-1`.
     */
    decrementingID: number;
    /**
     * Contains all same-kind, same-hierarchy-level `extends` chains
     * of all items.
     *
     * Relies on {@link sortByHierarchy} in order to be filled.
     */
    hierarchyChains: Chain<TypeDocDescriptor>;
    constructor(json: object);
    /** Convert every "kind" value (number/enum) to its actual string representation. */
    replaceReflectionKind(): void;
    /**
     * Replace the default `__namedParameters` parameter name for
     * rest parameters that had no name with a better one.
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link replaceReflectionKind}.
     */
    renameNamedParameters(newName?: string): void;
    /**
     * Mark a declaration as internal by adding the flag `_isInternal` to
     * it, iff all of its signatures are marked as @\internal.
     */
    markDeclarationsInternal(): void;
    /**
     * Order objects of kind "TypeAlias" to the top of a namespace's
     * and module's children.
     *
     * This is to ensure readability as a type alias does not have
     * a definitive header in the Nunjucks template.
     *
     * This would also be resolved automatically when TypeDoc's
     * kind order setting is set to order type aliases on top.
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link replaceReflectionKind}.
     */
    orderTypeAliasToTop(): void;
    /**
     * Give every "<internal>" module a unique name by an incrementing ID
     * so that they can be referenced without collision.
     */
    numberMultipleInternals(): void;
    /**
     * Order and merge multiple signatures in which only one signature
     * carries a comment such that only the last signature carries
     * the comment.
     */
    mergeSignatureDocs(): void;
    /**
     * Extend every {@link TypeDocDescriptor} by helper properties
     * and the actual object reference (as described in the interface).
     *
     * The data is stored in {@link targetByID}, indexed by the object id.
     */
    resolveDescriptors(): void;
    /**
     * Create a children collection for classes and interfaces
     * that is keyed by children variants, additionally making a
     * distinction between static and non-static items.
     *
     * This is utilized when building the outline in order to group all
     * such items by their variants without having to rely on TypeDoc's
     * (user-customizable) ordering.
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link replaceReflectionKind}.
     */
    addVariantFieldCollection(): void;
    /**
     * Constructs hierarchy chains of all reflections that are of the same kind
     * and have the same parent.
     *
     * Then, every chain's items are deleted and re-added (speak: sorted)
     * at the index of the first item to match their hierarchy.
     *
     * @example
     * Initial reflection ordering:
     * C1 extends C4; C2 extends C6; C3; C4; C5 extends C6; C6
     *
     * Constructed hierarchy chain(s):
     * [ C1, C4 ]
     *   +----^
     * [ C2, C5, C6 ]
     *   +---+----^
     *
     * After re-ordering:
     * C1 extends C4; C4; C2 extends C6, C5 extends C6; C6; C3; C4
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link resolveDescriptors} and
     * {@link orderExtendedBy}
     * (and thus, {@link replaceReflectionKind})
     *
     * @see {@link Chain}
     * @see {@link hierarchyChains}
     */
    sortByHierarchy(): void;
    /**
     * Resolve the inheritance such that every item is mapped
     * to its greatest parent or itself if it doesn't have a parent.
     * The mapping is represented by {@link targetByID}.
     *
     * This is done by collecting every "inheritance chain" over all objects.
     * An inheritance chain is a representation of the linear child-parent hierachy.
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link resolveDescriptors}
     */
    resolveInheritance(): void;
    /**
     * Replace the default `extendedTypes` hierarchy
     * with one that's indexed by its target's kinds.
     *
     * Before:
     * ```js
     * extendedTypes: [
     *   { target: 1 },
     *   { target: 2 },
     *   { target: 3 }
     * ]
     * ```
     * After (where target 1 and 3 are of kind "Class"
     * and target 2 is of kind "Interface"):
     * ```js
     * extendedTypesCollection: {
     *   Class: [ { target: 1 }, { target: 3 } ]
     *   Interface: [ { target: 2 } ],
     * }
     * ```
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link resolveInheritance} and
     * {@link replaceReflectionKind}.
     *
     * @see
     * https://typedoc.org/api/classes/Models.DeclarationReflection.html#extendedTypes
     */
    orderExtendedBy(): void;
    /**
     * Replace the default project children hierarchy with one that's indexed by kinds.
     *
     * Before:
     * ```js
     * children: [
     *   { kind: 256, id: 1 },
     *   { kind: 256, id: 2 },
     *   { kind: 128, id: 3 }
     * ]
     * ```
     * After (along with {@link replaceReflectionKind},
     * where 256 = "Interface" and 128 = "Class"):
     * ```js
     * childrenCollection: {
     *   Interface: [ { id: 1 }, { id: 2 } ],
     *   Class: [ { id: 3 } ]
     * }
     * ```
     *
     * @remarks
     * **ORDERING**: Needs to be called after {@link replaceReflectionKind}.
     */
    orderProjectChildren(): void;
    /**
     * Take an array and return an object of arrays.
     *
     * Every array child is queried for an arbitrary (user defined) key string –
     * Children with the same keys get collected in the same array
     * which is the value of the key.
     *
     * @param array The array to be converted to a collection.
     * @param keyGetter The key query. Is called with every child –
     *                  its return value is used as collection key.
     *
     * @see
     * {@link orderProjectChildren} and
     * {@link orderExtendedBy} for examples of this!
     */
    static getKeyCollection(array: Array<any>, keyGetter: (child: object) => string): Record<string, object[]>;
    /**
     * Recursively iterate over an arbitrary object, calling the
     * callback on all objects in order of appearance (no arrays)*.
     *
     * *This assumes that the input is of JSON format, thus not containing
     * arbitrary JS objects like Regexp.
     *
     * @see {@link MutationIteratorResult} for the passed object.
     *
     * @param item Any object up for recursive iteration.
     * @param callback The function that is called with each iteration.
     * @param ignore Ignore a field from iteration.
     *
     *     Think about adding a value here when you're adding one or
     *     multiple fields to an object.
     *     This helps prevent potential infinite loops and improves performance.
     */
    static iterateJSON(item: any, callback: MutationIteratorCallback, ignore?: string[]): void;
}
export {};
