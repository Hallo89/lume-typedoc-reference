import { Chain } from './Chain.js';
/** Literally copied from [the TypeDoc GitHub](https://raw.githubusercontent.com/TypeStrong/typedoc/v0.25.13/src/lib/models/reflections/kind.ts) */
export var ReflectionKind;
(function (ReflectionKind) {
    ReflectionKind[ReflectionKind["Project"] = 1] = "Project";
    ReflectionKind[ReflectionKind["Module"] = 2] = "Module";
    ReflectionKind[ReflectionKind["Namespace"] = 4] = "Namespace";
    ReflectionKind[ReflectionKind["Enum"] = 8] = "Enum";
    ReflectionKind[ReflectionKind["EnumMember"] = 16] = "EnumMember";
    ReflectionKind[ReflectionKind["Variable"] = 32] = "Variable";
    ReflectionKind[ReflectionKind["Function"] = 64] = "Function";
    ReflectionKind[ReflectionKind["Class"] = 128] = "Class";
    ReflectionKind[ReflectionKind["Interface"] = 256] = "Interface";
    ReflectionKind[ReflectionKind["Constructor"] = 512] = "Constructor";
    ReflectionKind[ReflectionKind["Property"] = 1024] = "Property";
    ReflectionKind[ReflectionKind["Method"] = 2048] = "Method";
    ReflectionKind[ReflectionKind["CallSignature"] = 4096] = "CallSignature";
    ReflectionKind[ReflectionKind["IndexSignature"] = 8192] = "IndexSignature";
    ReflectionKind[ReflectionKind["ConstructorSignature"] = 16384] = "ConstructorSignature";
    ReflectionKind[ReflectionKind["Parameter"] = 32768] = "Parameter";
    ReflectionKind[ReflectionKind["TypeLiteral"] = 65536] = "TypeLiteral";
    ReflectionKind[ReflectionKind["TypeParameter"] = 131072] = "TypeParameter";
    ReflectionKind[ReflectionKind["Accessor"] = 262144] = "Accessor";
    ReflectionKind[ReflectionKind["GetSignature"] = 524288] = "GetSignature";
    ReflectionKind[ReflectionKind["SetSignature"] = 1048576] = "SetSignature";
    ReflectionKind[ReflectionKind["TypeAlias"] = 2097152] = "TypeAlias";
    ReflectionKind[ReflectionKind["Reference"] = 4194304] = "Reference";
})(ReflectionKind || (ReflectionKind = {}));
;
/** Contains methods that mutate the typedoc JSON object. */
export class TypedocJSONMutator {
    json;
    /** Contains all {@link TypeDocDescriptor}s indexed by their ID. */
    targetByID = new Map();
    /**
     * ID that can be used when registering new reflections.
     * Is currently unused but might prove useful, who knows.
     *
     * @remarks
     * It is your responsibility to decrement it after using it!
     *
     * @remarks
     * It starts this low because TypeDoc itself uses the id `-1`.
     */
    decrementingID = -8192;
    /**
     * Contains all same-kind, same-hierarchy-level `extends` chains
     * of all items.
     *
     * Relies on {@link sortByHierarchy} in order to be filled.
     */
    hierarchyChains = new Chain();
    constructor(json) {
        this.json = json;
    }
    /** Convert every "kind" value (number/enum) to its actual string representation. */
    replaceReflectionKind() {
        TypedocJSONMutator.iterateJSON(this.json, ({ item }) => {
            if (typeof item.kind === 'number') {
                item.kind = ReflectionKind[item.kind];
            }
        });
    }
    /**
     * Replace the default `__namedParameters` parameter name for
     * rest parameters that had no name with a better one.
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link replaceReflectionKind}.
     */
    renameNamedParameters(newName = '_args') {
        TypedocJSONMutator.iterateJSON(this.json, ({ item }) => {
            if (item.name === '__namedParameters') {
                item.name = newName;
            }
        });
    }
    /**
     * Mark a declaration as internal by adding the flag `_isInternal` to
     * it, iff all of its signatures are marked as @\internal.
     */
    markDeclarationsInternal() {
        TypedocJSONMutator.iterateJSON(this.json, ({ item }) => {
            if (item.variant === 'declaration') {
                let isInternal = false;
                if (item.signatures) {
                    isInternal = item.signatures.every(handleItem);
                }
                else {
                    isInternal = handleItem(item);
                }
                item.flags._isInternal = isInternal;
            }
        });
        function handleItem(atomicItem) {
            return atomicItem?.comment?.modifierTags?.includes('@internal');
        }
    }
    /**
     * Order objects of kind "TypeAlias" to the top of a namespace's
     * and module's children.
     *
     * This is to ensure readability as a type alias does not have
     * a definitive header in the Nunjucks template.
     *
     * This would also be resolved automatically when TypeDoc's
     * kind order setting is set to order type aliases on top.
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link replaceReflectionKind}.
     */
    orderTypeAliasToTop() {
        TypedocJSONMutator.#JSONIterator(this.json, ({ item }) => {
            if (item.children && (item.kind === 'Project' || item.kind === 'Namespace' || item.kind === 'Module')) {
                item.children.sort((a, b) => {
                    return a.kind === 'TypeAlias' && b.kind !== 'TypeAlias' ? -1 : 1;
                });
            }
        });
    }
    /**
     * Give every "<internal>" module a unique name by an incrementing ID
     * so that they can be referenced without collision.
     */
    numberMultipleInternals() {
        let counter = 1;
        TypedocJSONMutator.iterateJSON(this.json, ({ item }) => {
            if (item?.kind === 'Module' && item.name === '<internal>') {
                item.name = `<internal:${counter}>`;
                counter++;
            }
        });
    }
    /**
     * Order and merge multiple signatures in which only one signature
     * carries a comment such that only the last signature carries
     * the comment.
     */
    mergeSignatureDocs() {
        TypedocJSONMutator.iterateJSON(this.json, ({ item }) => {
            if (item.signatures?.length > 1) {
                let signatureWithComment;
                for (const sig of item.signatures) {
                    if (sig.comment) {
                        if (signatureWithComment)
                            break;
                        signatureWithComment = sig;
                    }
                }
                if (signatureWithComment) {
                    const index = item.signatures.indexOf(signatureWithComment);
                    if (index !== item.signatures.length - 1) {
                        item.signatures.splice(index, 1);
                        item.signatures.push(signatureWithComment);
                    }
                }
            }
        });
    }
    /**
     * Extend every {@link TypeDocDescriptor} by helper properties
     * and the actual object reference (as described in the interface).
     *
     * The data is stored in {@link targetByID}, indexed by the object id.
     */
    resolveDescriptors() {
        const symbolIdMap = this.json.symbolIdMap;
        delete this.json.symbolIdMap;
        const uncheckedKinds = ['Constructor', '_TypeGroup'];
        TypedocJSONMutator.iterateJSON(this.json, ({ item, parentObject, parentItem }) => {
            // For some reason, all constructors of classes
            // that don't extend anything are not in the symbolIdMap
            if ('id' in item && parentObject && (item.id in symbolIdMap || uncheckedKinds.includes(item.kind))) {
                const data = (symbolIdMap[item.id] || {});
                if (uncheckedKinds.includes(item.kind)) {
                    data.customName = item.kind.toLowerCase();
                }
                if (parentObject?.variant !== 'project') {
                    data.parentName = parentObject.inheritanceName ?? parentObject.name ?? '';
                    if (parentObject.kind === 'Namespace' || parentObject.kind === 'Module') {
                        item.inheritanceName = data.parentName + '.' + item.name;
                    }
                }
                data.parentReference = parentItem;
                data.reference = item;
                this.targetByID.set(item.id, data);
            }
        });
    }
    /**
     * Create a children collection for classes and interfaces
     * that is keyed by children variants, additionally making a
     * distinction between static and non-static items.
     *
     * This is utilized when building the outline in order to group all
     * such items by their variants without having to rely on TypeDoc's
     * (user-customizable) ordering.
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link replaceReflectionKind}.
     */
    addVariantFieldCollection() {
        TypedocJSONMutator.iterateJSON(this.json, ({ item }) => {
            // I hope this covers most of it?
            if (item && item.children && (item.kind === 'Class'
                || item.kind === 'Interface'
                || item.kind === 'Namespace'
                || item.kind === 'Module')) {
                const children = (item.kind === 'Namespace' || item.kind === 'Module')
                    ? item.children.filter(child => child.kind === 'TypeAlias')
                    : item.children;
                const collection = TypedocJSONMutator.getKeyCollection(children, child => {
                    if (child.flags?.isStatic) {
                        return 'Static ' + child.kind.toLowerCase();
                    }
                    return child.kind;
                });
                item._variantCollection = collection;
            }
        }, ['_variantCollection']);
    }
    /**
     * Constructs hierarchy chains of all reflections that are of the same kind
     * and have the same parent.
     *
     * Then, every chain's items are deleted and re-added (speak: sorted)
     * at the index of the first item to match their hierarchy.
     *
     * @example
     * Initial reflection ordering:
     * C1 extends C4; C2 extends C6; C3; C4; C5 extends C6; C6
     *
     * Constructed hierarchy chain(s):
     * [ C1, C4 ]
     *   +----^
     * [ C2, C5, C6 ]
     *   +---+----^
     *
     * After re-ordering:
     * C1 extends C4; C4; C2 extends C6, C5 extends C6; C6; C3; C4
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link resolveDescriptors} and
     * {@link orderExtendedBy}
     * (and thus, {@link replaceReflectionKind})
     *
     * @see {@link Chain}
     * @see {@link hierarchyChains}
     */
    sortByHierarchy() {
        const chainData = new WeakMap;
        TypedocJSONMutator.iterateJSON(this.json, ({ item, parentItem }) => {
            if ('extendedTypesCollection' in item && item.kind in item.extendedTypesCollection) {
                const itemData = this.targetByID.get(item.id);
                let extendedData = null;
                // Search for an extended item (id) that is of the same kind and has the same parent.
                for (const hierarchyItem of item.extendedTypesCollection[item.kind]) {
                    if (typeof hierarchyItem.target !== 'number')
                        continue;
                    const data = this.targetByID.get(hierarchyItem.target);
                    if (data.parentReference === itemData.parentReference) {
                        extendedData = data;
                        break;
                    }
                }
                if (extendedData != null) {
                    const chain = this.hierarchyChains.add(itemData, extendedData);
                    const chainProperties = chainData.get(chain);
                    const itemIndex = Math.min(parentItem.indexOf(item), parentItem.indexOf(extendedData.reference));
                    if (!chainProperties) {
                        chainData.set(chain, {
                            smallestIndex: itemIndex,
                            parentItem
                        });
                    }
                    else if (chainProperties.smallestIndex > itemIndex) {
                        chainProperties.smallestIndex = itemIndex;
                    }
                    item._extendsBelow = true;
                }
            }
        });
        for (const chain of this.hierarchyChains.get()) {
            const chainProperties = chainData.get(chain);
            const parent = chainProperties.parentItem;
            const kind = chain[0].reference.kind;
            for (const child of Array.from(parent)) {
                if (chain.some(chainItem => chainItem.reference === child)) {
                    parent.splice(parent.indexOf(child), 1);
                }
            }
            // Mark the item that is being inherited
            chain.at(-1).reference._lastExtendTarget = true;
            // If more than one item inherits the same item, mark them.
            let lastAnchorData;
            for (let i = chain.length - 1; i >= 0; i--) {
                const itemData = chain[i];
                const lastItemData = chain[i + 1];
                const collection = itemData.reference.extendedTypesCollection?.[kind];
                if (!collection || !lastAnchorData || !collection.some(entry => entry.target === lastAnchorData.reference.id)) {
                    lastAnchorData = itemData;
                }
                else if (lastAnchorData !== lastItemData) {
                    itemData.reference._extendsFurtherBelow = true;
                }
            }
            const chainReferences = chain.map(chainItem => chainItem.reference);
            parent.splice(chainProperties.smallestIndex, 0, ...chainReferences);
        }
    }
    /**
     * Resolve the inheritance such that every item is mapped
     * to its greatest parent or itself if it doesn't have a parent.
     * The mapping is represented by {@link targetByID}.
     *
     * This is done by collecting every "inheritance chain" over all objects.
     * An inheritance chain is a representation of the linear child-parent hierachy.
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link resolveDescriptors}
     */
    resolveInheritance() {
        const inheritance = new Chain();
        TypedocJSONMutator.iterateJSON(this.json, ({ item, parentItem, itemKey }) => {
            if ('inheritedFrom' in item) {
                const itemID = item.id;
                const parentID = item.inheritedFrom.target;
                inheritance.add(itemID, parentID);
                // NOTE: These lines delete any inherited item:
                // if (Array.isArray(parentItem)) {
                //   parentItem.splice(parentItem.indexOf(item), 1);
                // } else {
                //   delete parentItem[itemKey];
                // }
            }
        });
        for (const chain of inheritance.get()) {
            const targetID = chain.at(-1);
            const targetObject = this.targetByID.get(targetID);
            for (const id of chain) {
                this.targetByID.set(id, targetObject);
            }
        }
    }
    /**
     * Replace the default `extendedTypes` hierarchy
     * with one that's indexed by its target's kinds.
     *
     * Before:
     * ```js
     * extendedTypes: [
     *   { target: 1 },
     *   { target: 2 },
     *   { target: 3 }
     * ]
     * ```
     * After (where target 1 and 3 are of kind "Class"
     * and target 2 is of kind "Interface"):
     * ```js
     * extendedTypesCollection: {
     *   Class: [ { target: 1 }, { target: 3 } ]
     *   Interface: [ { target: 2 } ],
     * }
     * ```
     *
     * @remarks
     * **ORDERING**: Needs to be called after
     * {@link resolveInheritance} and
     * {@link replaceReflectionKind}.
     *
     * @see
     * https://typedoc.org/api/classes/Models.DeclarationReflection.html#extendedTypes
     */
    orderExtendedBy() {
        TypedocJSONMutator.iterateJSON(this.json, ({ item }) => {
            if ('extendedTypes' in item) {
                const collection = TypedocJSONMutator.getKeyCollection(item.extendedTypes, child => {
                    // 'Class' as a damage reduction-y fallback –
                    // No information is available to get the kind.
                    return this.targetByID.get(child.target)?.reference.kind || 'Class';
                });
                delete item.extendedTypes;
                item.extendedTypesCollection = collection;
            }
        }, ['extendedTypesCollection']);
    }
    /**
     * Replace the default project children hierarchy with one that's indexed by kinds.
     *
     * Before:
     * ```js
     * children: [
     *   { kind: 256, id: 1 },
     *   { kind: 256, id: 2 },
     *   { kind: 128, id: 3 }
     * ]
     * ```
     * After (along with {@link replaceReflectionKind},
     * where 256 = "Interface" and 128 = "Class"):
     * ```js
     * childrenCollection: {
     *   Interface: [ { id: 1 }, { id: 2 } ],
     *   Class: [ { id: 3 } ]
     * }
     * ```
     *
     * @remarks
     * **ORDERING**: Needs to be called after {@link replaceReflectionKind}.
     */
    orderProjectChildren() {
        TypedocJSONMutator.iterateJSON(this.json, ({ item }) => {
            if (item && (item.variant === 'project' && Array.isArray(item.children))) {
                const collection = TypedocJSONMutator.getKeyCollection(item.children, child => child.kind);
                delete item.children;
                item.childrenCollection = collection;
            }
        }, ['childrenCollection']);
    }
    // ---- Static misc helpers ----
    /**
     * Take an array and return an object of arrays.
     *
     * Every array child is queried for an arbitrary (user defined) key string –
     * Children with the same keys get collected in the same array
     * which is the value of the key.
     *
     * @param array The array to be converted to a collection.
     * @param keyGetter The key query. Is called with every child –
     *                  its return value is used as collection key.
     *
     * @see
     * {@link orderProjectChildren} and
     * {@link orderExtendedBy} for examples of this!
     */
    static getKeyCollection(array, keyGetter) {
        const keyCollection = {};
        for (const child of array) {
            const key = keyGetter(child);
            if (!(key in keyCollection)) {
                keyCollection[key] = [];
            }
            keyCollection[key].push(child);
        }
        return keyCollection;
    }
    // ---- Static iteration helper ----
    /**
     * Recursively iterate over an arbitrary object, calling the
     * callback on all objects in order of appearance (no arrays)*.
     *
     * *This assumes that the input is of JSON format, thus not containing
     * arbitrary JS objects like Regexp.
     *
     * @see {@link MutationIteratorResult} for the passed object.
     *
     * @param item Any object up for recursive iteration.
     * @param callback The function that is called with each iteration.
     * @param ignore Ignore a field from iteration.
     *
     *     Think about adding a value here when you're adding one or
     *     multiple fields to an object.
     *     This helps prevent potential infinite loops and improves performance.
     */
    static iterateJSON(item, callback, ignore = []) {
        this.#JSONIterator(item, callback, ignore);
    }
    static #JSONIterator(item, callback, ignore = [], itemKey = '', parentItem = null, parentObject = null) {
        if (item == null || typeof item !== 'object')
            return;
        if (!Array.isArray(item)) {
            callback({ item, itemKey, parentItem, parentObject });
        }
        for (const [name, child] of Object.entries(item)) {
            if (!ignore.includes(name)) {
                this.#JSONIterator(child, callback, ignore, name, item, Array.isArray(item) ? parentObject : item);
            }
        }
    }
}
