import type { TypeDocDescriptor } from './TypedocJSONMutator.js';
import { TypedocJSONMutator } from './TypedocJSONMutator.js';
import { IndentationHandler } from './IndentationHandler.js';
/**
 * Handles the necessary operations needed for a template to be built.
 * Significant fields:
 * - A TypeDoc object that is mutated *in place* to fit the template's needs.
 * - A new Nunjucks environment with all filters needed by the template
 * @see {@link TypedocJSONMutator}
 * @see {@link IndentationHandler}
 */
export declare class Environment {
    #private;
    /**
     * Controls the default visibility options (exposed through the checkboxes
     * on the top right) of the resulting documentation front-end.
     * @default
     * {
     *   private: false,
     *   internal: false,
     *   inherited: false,
     * }
     */
    visibility: {
        /** Show or hide all private fields. */
        private: boolean;
        /** Show or hide all fields marked with the "@internal" tag. */
        internal: boolean;
        /** Show or hide all inherited fields. */
        inherited: boolean;
    };
    /**
     * A new [Nunjucks](https://mozilla.github.io/nunjucks/api.html) environment
     * that's used to compile the reference template.
     */
    njk: any;
    /**
     * A new [Markdown-It](https://github.com/markdown-it/markdown-it) environment
     * that's used to render inline markdown via the "md" filter.
     */
    md: any;
    /**
     * The supplied TypeDoc JSON as JavaScript object.
     *
     * *IMPORTANT*: It is heavily mutated *in place*, so if you need to use the
     * intact object after building a doc from it, you will need to make a deep copy.
     */
    json: object;
    mutator: TypedocJSONMutator;
    indentHandler: IndentationHandler;
    constructor(json: object, nunjucksDirPath: string);
    /** @see {@link Controller.build} */
    render(): string;
    /** @see {@link Controller.buildMain} */
    renderMain(): string;
    /** @see {@link Controller.buildOverview} */
    renderOverview(): string;
    /** @see {@link Controller.buildOutline} */
    renderOutline(): string;
    /**
     * Call all predefined mutators. The order is important.
     * @see {@link TypedocJSONMutator}
     */
    mutateJSON(): void;
    /** Add filters that are mandatory for the pre-defined Nunjucks templates to work. */
    addMandatoryFilters(): void;
    /** Get the target descriptor for the specified ID. */
    resolveTarget(target: number): TypeDocDescriptor | undefined;
}
