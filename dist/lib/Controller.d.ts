import { Environment } from './Environment.js';
export interface Options {
    /**
     * A TypeDoc JSON as JS object.
     * It serves as the default JSON the docs will be built from
     * when not passing a TypeDoc object to the `reference.doc()` component.
     *
     * *note* that when no default JSON is defined,
     * passing no argument to `reference.doc()` will result in an error.
     *
     * *IMPORTANT*: This object is not copied and WILL get heavily mutated.
     * If you need to reuse it afterwards in its original form, you will
     * have to make a deep copy.
     */
    defaultJSON?: object;
    /**
     * The destination path of the trdoc CSS file, relative to your directory.
     *
     * It is the responsibility of the implementer (e.g. an SSG plugin)
     * to utilize this option by copying the {@link Controller.cssFilePath}
     * over to the given path ({@link Controller.cssDestFile}).
     *
     * trdoc has no hard dependency on any CSS file. As such, it is the
     * user's responsibility to include it in the document.
     *
     * If not supplied, nothing will be copied over – you will either
     * have to copy it yourself or write your own style sheet.
     */
    cssDestFile?: string;
    /**
     * Is called as the last operation in the {@link Controller}
     * with the controller instance itself as parameter.
     *
     * Use it to change any property of the Controller,
     * which is the main entry point for all operations.
     */
    init?(controller: Controller): void;
    /**
     * Is called upon creating a new {@link Environment}, passed as argument.
     *
     * Use it to modify the environment, for example to mutate its TypeDoc object,
     * configure the indentation or do things with the Nunjucks environment.
     *
     * @example
     * Change an evironment's default visibility options (the checkboxes on
     * the top right) such that all private fields are shown by default:
     * ```js
     * env => {
     *   env.visibility.private = true;
     * }
     * ```
     *
     * @see {@link IndentationHandler}
     * @see {@link TypedocJSONMutator}
     */
    envInit?(env: Environment): void;
}
/** Path to the trdoc project root */
export declare const PROJECT_ROOT: string;
/** Handles JSON mutation, Lume environments, DOM parsing, etc. */
export declare class Controller {
    /** @see {@link Options.cssDestFile} */
    cssDestFile?: string;
    /** local path to the folder containing all Nunjucks templates. */
    nunjucksDirPath: string;
    /** local path to the trdoc CSS file. */
    cssFilePath: string;
    /** @see {@link Options.envInit} */
    envInit: Options['envInit'];
    /** A {@link Environment} created the potentially supplied {@link Options.defaultJSON}. */
    defaultEnv: Environment | undefined;
    constructor(config?: Options);
    /**
     * Get the rendered HTML string from the supplied TypeDoc object
     * or the configured default object if not supplied.
     *
     * This will render the actual reference in a <main> and
     * both the overview and the outline in an <aside>.
     * @see {@link Options.defaultJSON}
     * @param json A TypeDoc JSON as JavaScript object.
     */
    build(json?: object): string;
    /** Same as {@link build}, but only render the main part of the reference. */
    buildMain(json?: object): string;
    /** Same as {@link build}, but only render the reference overview. */
    buildOverview(json?: object): string;
    /** Same as {@link build}, but only render the reference outline. */
    buildOutline(json?: object): string;
    /**
     * Get the rendered HTML string from a TypeDoc JSON file.
     * @param content The un-parsed content of the TypeDoc JSON file.
     */
    buildFromFileContent(content: string): string;
    /**
     * Lume [page loader](https://lume.land/docs/core/loaders/),
     * rendering the contents of the supplied TypeDoc JSON path.
     * @param content The un-parsed content of the TypeDoc JSON file.
     */
    jsonPageLoader(content: string): object;
    /**
     * Create an {@link Environment} with the supplied TypeDoc object as base.
     * Calls {@link envInit} with the created environment for further customization.
     * @param json The TypeDoc JSON as JavaScript object.
     */
    createEnvironment(json: object): Environment;
    /**
     * Get either a newly created {@link Environment} from the supplied TypeDoc
     * object or return the default Environment if nothing is passed.
     * @throws Throws if nothing has been passed and no default Env exists.
     * @param json The TypeDoc JSON as JavaScript object.
     */
    getEnv(json?: object): Environment;
}
