import { default as HTMLParser, HTMLElement, TextNode } from 'node-html-parser';
/**
 * Contains methods to dynamically wrap & indent specific DOM elements
 * (namely containers like arrays, objects, etc.) once they exceed a specified width.
 * @see {@link indentLengthThreshold} for a way to modify the tested length.
 */
export class IndentationHandler {
    /**
     * Classes that should be indented.
     * Is prioritized from top to bottom.
     */
    static indentableBodies = [
        'signature',
        'parameter-list',
        'type-param-list',
        'conditional-body',
        'reflection-body',
        'tuple-body',
        'generic-body',
    ];
    /**
     * Classes that don't depend on added breaks in between
     * their child elements.
     */
    static independentBodies = [
        'conditional-body',
        'generic-body',
        'signature',
    ];
    /**
     * Classes that may be collapsed, e.g. the array in
     * `[{ index: 1 }]`
     */
    static collapsibleBodies = [
        'parameter-list',
        'type-param-list',
        'tuple-body',
        'generic-body',
    ];
    /**
     * Character length threshold dictating above which length
     * a method should be broken up and indented.
     */
    indentLengthThreshold = 62;
    /**
     * Character length threshold dictating when an indent should
     * be avoided if its content is of up to this length.
     */
    indentAvoidThreshold = 4;
    constructor() {
        this.addIndent = this.addIndent.bind(this);
    }
    /**
     * Add indent to the specified text content.
     * The text content must be a valid, parsable HTML string.
     */
    addIndent(content) {
        const node = IndentationHandler.modifyStringAndCreateElement(content.val);
        bodyLoop: for (const indentClass of IndentationHandler.indentableBodies) {
            while (true) {
                const breakableLines = IndentationHandler.getBreakableLines(node.textContent, this.indentLengthThreshold);
                if (breakableLines.length === 0) {
                    break bodyLoop;
                }
                if (!IndentationHandler.recursivelyIndentNode(node, indentClass, this.indentAvoidThreshold, breakableLines)) {
                    break;
                }
            }
        }
        return IndentationHandler.postProcessHTMLString(node.innerHTML);
    }
    static recursivelyIndentNode(node, currentClass, avoidThreshold, lines) {
        if (this.childElementCount(node) === 0 || node instanceof TextNode)
            return;
        if (node.classList?.contains(currentClass)
            && lines.some(line => line.includes(node.textContent))
            && node.textContent.length > avoidThreshold
            && !node.textContent.includes('\n')
            && !node.parentElement?.classList.contains('indent')
            && (
            // Skip indentation if the only grandchild is already an indent,
            // collapsing the indentation, e.g. `[{ index: 1 }]`.
            this.childElementCount(node) > 1
                || !this.isCollapsibleBody(this.firstElementChild(this.firstElementChild(node))))) {
            this.addIndentationWrapper(node);
            if (!this.isIndependentBody(node)) {
                this.addIndentationBreaks(node);
            }
        }
        for (const child of this.children(node)) {
            this.recursivelyIndentNode(child, currentClass, avoidThreshold, lines);
        }
    }
    // ---- Element queries ----
    /** Checks whether an element is indentable as per {@link indentableBodies} */
    static isBody(element) {
        return this.#classListContains(element, this.indentableBodies);
    }
    /** Checks whether an element is independent as per {@link independentBodies} */
    static isIndependentBody(element) {
        return this.#classListContains(element, this.independentBodies);
    }
    /** Checks whether an element is collapsible as per {@link collapsibleBodies} */
    static isCollapsibleBody(element) {
        return this.#classListContains(element, this.collapsibleBodies);
    }
    // ---- Element mutation ----
    /** Insert a <br> element between each of the supplied element's children. */
    static addIndentationBreaks(parentElement) {
        // NOTE: It is necessary to copy `children` here, otherwise
        //       it will create an infinite loop since `children` is live.
        for (const child of Array.from(this.children(parentElement))) {
            if (child !== this.lastElementChild(parentElement)) {
                this.after(child, this.createElement('br'));
                this.after(child, this.createTextNode('\n'));
            }
        }
    }
    /** Wrap the child content of the supplied element in an `<div class=indent>` tag. */
    static addIndentationWrapper(element) {
        const node = this.createElement('div', 'indent');
        element.parentNode.exchangeChild(element, node);
        node.appendChild(element);
        this.before(element, this.createTextNode('\n'));
        this.after(element, this.createTextNode('\n'));
    }
    // ---- Helpers ----
    /**
     * Get a list of all lines that go over the line length threshold
     * from an arbitrary string input.
     */
    static getBreakableLines(input, threshold) {
        return input
            .split('\n')
            .map(line => line.trim())
            .filter(line => line.length >= threshold);
    }
    /**
     * Remove any newlines, insert newlines after <br> tags
     * and compress whitespace.
     *
     * This is done because the indenter uses newlines as markers to determine
     * line breaks. The whitespace is compressed to prevent length inaccuracies.
     */
    static getModifiedHTMLString(input) {
        return input
            .replaceAll(/\n/g, '')
            .replaceAll(/\s+/g, ' ')
            .replaceAll(/<br\s*\/?>/g, '<br>\n');
    }
    static postProcessHTMLString(input) {
        return input
            .replaceAll(/\n/g, '');
    }
    static modifyStringAndCreateElement(content) {
        content = this.getModifiedHTMLString(content);
        return HTMLParser.parse(content);
    }
    /**
     * Search the classList of the given element for matches
     * in the given string array.
     */
    static #classListContains(element, haystack) {
        return element && haystack.some(val => element.classList.contains(val));
    }
    // ---- html parser shims ----
    static before(element, insertedElem) {
        if (element instanceof HTMLElement && element.parentNode) {
            const siblings = element.parentNode.childNodes;
            siblings.splice(siblings.indexOf(element), 0, insertedElem);
            insertedElem.parentNode = element;
        }
    }
    static after(element, insertedElem) {
        if (element instanceof HTMLElement && element.parentNode) {
            const siblings = element.parentNode.childNodes;
            siblings.splice(siblings.indexOf(element) + 1, 0, insertedElem);
            insertedElem.parentNode = element;
        }
    }
    static createElement(type, classes) {
        return new HTMLElement(type, { class: classes });
    }
    static createTextNode(content) {
        return new TextNode(content);
    }
    /** Get all {@link HTMLElement} children. */
    static children(element) {
        return element.childNodes.filter(child => child instanceof HTMLElement);
    }
    static childElementCount(element) {
        return this.children(element).length;
    }
    static firstElementChild(element) {
        return this.children(element)[0];
    }
    static lastElementChild(element) {
        return this.children(element).at(-1);
    }
}
