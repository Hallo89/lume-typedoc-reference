/**
 * Simple helper class for resolving "hierarchy chains".
 *
 * A chain is a representation of a linear hierarchy; Every item can have
 * at most one parent and one child. Upon addition, they will be resolved
 * to a conclusive hierarchy chain, represented by an array.
 *
 * The representation of an item can be anything, but needs to be consistent
 * (generic class parameter T).
 */
export declare class Chain<T = number> {
    #private;
    /**
     * Add an item along with its parent.
     * The algorithm will add them to a (potentially pre-existing) chain.
     * @returns The chain that the items were added onto.
     *          NOTE that these chains may be deleted in successive calls to `add()`!
     *          It is good practice to use a WeakSet or WeakMap when storing them somewhere.
     */
    add(item: T, parent: T): T[];
    /** Get the set of all chains. */
    get(): Set<T[]>;
    /** Find the chain (or false) from a given item. */
    getChainFromItem(item: T): false | T[];
}
